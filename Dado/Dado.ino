//Declaracion de Variables
int leds[5] = {13,12,11,10,9}; // Arreglo donde indicara donde estan conectados los leds
int botonUP = 8 ;// Pin n8
int n = 0;
// Funciones
void TurnOnLed(int i); // Funciones que prende 


void setup()
{
  for(int i = 0; i < 5; i++)
  {
    pinMode(leds[i],OUTPUT);
  }

   pinMode(botonUP,INPUT_PULLUP);
}

void loop() 
{
  if(digitalRead(botonUP) == 0)
  {
    randomSeed(analogRead(10));
    n = random(5);
    TurnOnLed(n);
  }
}

void TurnOnLed(int i) 
{
  digitalWrite(leds[i],HIGH);
  delay(500); 
  digitalWrite(leds[i],LOW);
}



  
