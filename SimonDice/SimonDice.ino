//Variables
int leds[3] = {13,12,11};
int botonUP[3] = {7,6,5};
int seleccion = 4;

int secuencia[10];
int contador = 0;
int secuenciaMax = 7;

//Funciones
void WinOrLose();

//Setup & Loop

void setup() 
{

  for(int i = 0; i < 3; i++)
  {
    pinMode(botonUP[i],INPUT_PULLUP);
    pinMode(leds[i],OUTPUT);
  }
}

void loop() 
{
  MostrarSecuencia();
  LeerSecuencia();
}

void ShowCorrectLed(int c) // Funcion que muestra el led Correcto
{
    digitalWrite(leds[c],HIGH);
    delay(250);
    digitalWrite(leds[c],LOW);
    delay(250);
}

void MostrarSecuencia() // Funcion que recorre la secuencia y la muestra utilizando la funcion anterior
{
  randomSeed(analogRead(8));

  secuencia[contador] = random(3);
  for(int i = 0; i<contador; i++)
  {
    ShowCorrectLed(secuencia[i]);
  }
  delay(400);
  contador ++;
}

void LeerSecuencia()// Funcion que espera que se precione algun boton para verificar si se preciono correctamente siguiendo la secuencia
{
  for(int i = 1; i < contador; i++)
  {
    while(seleccion == 4) // seleccion esta en 4 ya que el arreglo de botones es de 0-2
    {
      if(digitalRead(botonUP[0]) == 0)
      {
        seleccion = 0;
      }
      
      if(digitalRead(botonUP[1]) == 0)
      {
        seleccion = 1;
      }
      
      if(digitalRead(botonUP[2]) == 0)
      {
        seleccion = 2;
      }
     }
     if(secuencia[i-1] == seleccion)
     {
        ShowCorrectLed(seleccion);
        if(i == secuenciaMax)
        {
          WinOrLose();
        }
     }
     else // Si no se elige correctamente muestra el led 3 veces que seguia
     {
      delay(400);
      ShowCorrectLed(secuencia[i-1]);
      ShowCorrectLed(secuencia[i-1]);
      ShowCorrectLed(secuencia[i-1]);
      delay(400);
      WinOrLose();
     }
    seleccion = 4;
   }
   delay(100);
}



void WinOrLose()
{
  for(int i; i < 3; i++)
  {
     digitalWrite(leds[i],HIGH);
  }
  delay(2000);
  for(int i; i < 3; i++)
  {
     digitalWrite(leds[i],LOW);
  }
  contador = 0;
}
