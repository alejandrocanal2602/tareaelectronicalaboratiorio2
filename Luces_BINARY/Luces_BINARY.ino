//Declaracion de Variables
int leds[5] = {4,6,8,10,12};
char binario[5] = {'0','0','0','0','0'};
int botonUP = 3;
int botonDOWN = 2;
int valUP = 0;
int valDOWN = 0;
int n = 0;

//Funciones
void TurnOnLed(); // Prende un led pasando el indice
void ConvertToBinari(); // Convierte el un numero a binario

void setup() {
  
  pinMode(botonUP,INPUT_PULLUP);
  pinMode(botonDOWN,INPUT);
  
  for(int i = 0; i < 5; i++)
  {
    pinMode(leds[i],OUTPUT);
  }
}

void loop() 
{
  valUP = digitalRead(botonUP);
  valDOWN = digitalRead(botonDOWN);
  
  if(valUP == 0)// suma 1 a n y luego lo convierte a binario
  {
    n++;
    if(n > 31)
    {
      n = 0;
    }
    ConvertToBinari();
    
   delay(300);
  }
    if(valDOWN == 1)// Resta 1 a n y luego lo convierte a binario
  {
    n--;
    if(n < 0)
    {
      n = 0;
    }
    ConvertToBinari();
    
   delay(300);
  }
  TurnOnLed(); 
  
}

void TurnOnLed() // Decide que leds se prende recorriendo el arreglo
{
 for(int i = 0; i < 5; i++)
 {
  if(binario[i] == '1')
  {
    digitalWrite(leds[i],HIGH);
  }
  else
  {
    digitalWrite(leds[i],LOW);
  }
 }
}

void ConvertToBinari()
{
 
 int c = 0;
 int k = 0;
  for(c = 4; c >=0 ; c--)
  {
    k = n >> c; // Corre el numero c veces y da el valor en binario
    if(k&1) // Ejemplo: k = 00101  1 = 00001 -> TRUE 
    {
      binario[c] = '1';
    }
    else
    {
      binario[c] = '0';
    }
  }
}
